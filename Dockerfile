FROM \
    php:7.3.20-cli-alpine
RUN \
    apk add \
        --no-cache \
        --update \
        --virtual \
            .build-dependencies \
            autoconf \
            g++ \
            libxslt-dev \
            make \
    && \
    apk add \
        --no-cache \
            composer \
            git \
            gnu-libiconv \
            libxslt \
            util-linux \
    && \
    pecl install \
        xdebug \
    && \
    docker-php-ext-enable \
        xdebug \
    && \
    docker-php-ext-install \
        pdo_mysql \
        xsl \
    && \
    apk del \
        .build-dependencies \
    && \
    rm \
        -r \
        -f \
            /var/cache/apk/*
ENV \
    LD_PRELOAD \
    /usr/lib/preloadable_libiconv.so
COPY \
    /filesystem/ \
    /
