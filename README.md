# Corona

Corona is a Docker image based on Alpine Linux that contains Git Version Control System, Composer Dependency Manager,
PHP Command Line Interface and extensions.

## Authentication

Authenticate to the GitLab Container Registry before use this image.

```bash
docker login registry.gitlab.com -u <username> -p <token>
```

## Usage

Preferably, use this image in a Continuous Integration environment.

```yml
image: registry.gitlab.com/home-health/corona
stages:
    - install
    - test
cache:
    paths:
        - vendor
install:
    stage: install
    script:
        - composer install
test:
    stage: test
    script:
        - composer run test
```

## Contributing

See the [Contribution Guide](CONTRIBUTING.md).

## License

See the [End-User License Agreement (EULA)](LICENSE.md).
