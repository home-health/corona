# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com), and this project adheres to
[Semantic Versioning](https://semver.org).

## [Unreleased]

## [1.5.7] - 2020-07-13

### Changed
- Docker image based on PHP 7.3.20 CLI.

## [1.5.6] - 2020-07-04

### Changed
- Specification of Docker 19.03.12 as a Continuous Integration image.
- Specification of Docker DinD 19.03.12 as a Continuous Integration service.

## [1.5.5] - 2020-06-14

### Changed
- Docker image based on PHP 7.3.19 CLI.

## [1.5.4] - 2020-06-02

### Changed
- Specification of Docker 19.03.11 as a Continuous Integration image.
- Specification of Docker DinD 19.03.11 as a Continuous Integration service.

## [1.5.3] - 2020-05-27

### Changed
- Specification of Docker 19.03.9 as a Continuous Integration image.
- Specification of Docker DinD 19.03.9 as a Continuous Integration service.
- Docker image based on PHP 7.3.18 CLI.

## [1.5.2] - 2020-04-17

### Changed
- Docker image based on PHP 7.3.17 CLI.

## [1.5.1] - 2020-03-27

### Changed
- Specification of Docker 19.03.8 as a Continuous Integration image.
- Specification of Docker DinD 19.03.8 as a Continuous Integration service.
- Docker image based on PHP 7.3.16 CLI.

## [1.5.0] - 2020-03-13

### Added
- Random collection of Linux utilities.

### Changed
- Reorder commands in test script alphabetically.

## [1.4.1] - 2020-03-11

### Changed
- Specification of Docker 19.03.7 as a Continuous Integration image.
- Specification of Docker DinD 19.03.7 as a Continuous Integration service.
- Docker image based on PHP 7.3.15 CLI.
- Use apostrophe character for strings.

## [1.4.0] - 2020-02-24

### Added
- GNU charset conversion library.

## [1.3.6] - 2020-02-23

### Changed
- Reorder commands in test script alphabetically.
- Reorder attributes in Continuous Integration configuration file alphabetically.

## [1.3.5] - 2020-02-22

### Fixed
- Correct use of uppercase and lowercase letters.

## [1.3.4] - 2020-02-22

### Changed
- Project description.

## [1.3.3] - 2020-02-22

### Changed
- Project description.

### Added
- Git Version Control System.

## [1.3.2] - 2020-02-21

### Changed
- Specification of Docker 19.03.5 as a Continuous Integration image.
- Specification of Docker DinD 19.03.5 as a Continuous Integration service.

## [1.3.1] - 2020-02-21

### Changed
- Limit 120 characters per line.
- Indent lines with 4 spaces.

## [1.3.0] - 2020-02-20

### Added
- PHP extension to transform XML documents.

## [1.2.7] - 2020-02-12

### Fixed
- Continuous Integration file indenting.

## [1.2.6] - 2020-02-12

### Fixed
- Continuous Integration file reusability.
- Continuous Integration file verbosity.
- Continuous Integration file indenting.

### Removed
- Unnecessary script in Continuous Integration.

## [1.2.5] - 2020-02-10

### Changed
- Docker image based on PHP 7.3.14 CLI.

## [1.2.4] - 2020-02-10

### Changed
- Test the PHP version.

## [1.2.3] - 2020-02-10

### Changed
- Surround grep search terms with quotes.

## [1.2.2] - 2020-02-06

### Changed
- Use a directory to illustrate the filesystem.

## [1.2.1] - 2020-02-06

### Changed
- Test the list of compiled PHP modules to ensure extension installation.

### Fixed
- Authenticate in Container Registry before pulling an image to cache.

## [1.2.0] - 2020-02-06

### Added
- Test stage in Continuous Integration.
- Docker image build process with cache.
- Test script.

## [1.1.0] - 2020-02-05

### Added
- Debugger and profiler tool for PHP.

## [1.0.0] - 2020-02-05

### Added
- Project readme.
- Remove unnecessary files from Docker build context.
- Continuous Integration.
- Driver that implements PDO interface to enable access from PHP to MySQL.
- Command to clear installation cache.
- Composer as a package manager.
- Docker image based on Alpine Linux and PHP 7.3 CLI.
- Project license.
- Contributing guide.
- Record of all notable changes made to this project.
